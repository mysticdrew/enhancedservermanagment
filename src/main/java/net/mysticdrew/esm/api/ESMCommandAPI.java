/*	EnhancedServerModeration is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    EnhancedServerModeration is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with EnhancedServerModeration.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.mysticdrew.esm.api;

import java.lang.reflect.Method;

import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;

/**
 * This class is the main interface for the api to talk to the core of ESM.
 * To use the API, your command must use EsmCommandBase as the superclass for your command.
 * Your command class must also implement IEsmCommand. 
 * To register your command with ESM you need to add RegisterEsmCommand.registerCommand(event, new SomeCommand()) to your
 * FMLServerStartingEvent event. Like you would with a normal command. 
 * If EnhancedServerModeration is not found in the mods folder, your command will be registered and function
 * like a normal command. 
 * Once EnhancedServerModeration is added to the mods folder, ESM will handled your command.
 * @author Mysticdrew
 *
 */
public final class ESMCommandAPI {
	
//	private static Method getRequiredApiVersion;
//	private static Class esmCommandCore;
//	private static Class playerUtils;
//	private static Method canPlayerUseCommand;
//	private static Method getRequiredPermLevel;
//	private static Method isOp;
//	private static boolean searched;
//		
//	/**
//	 * This method sends the command sender and the commandId to ESM to get verification of the user
//	 * has access to the command. 
//	 * @param sender
//	 * @param id
//	 * @return true or false depending if the user can use the command.
//	 */
//	public static boolean canCommandSenderUseCommand(ICommandSender sender, String id) {		
//		findMethods();
//		if (canPlayerUseCommand != null) {
//			try {
//				return (Boolean)canPlayerUseCommand.invoke(null, sender, id);
//			} catch (Exception e) {
//				
//			}
//		}
//		return isOp(sender);
//	}
//	
//	/**
//	 * This method checks to see if the user is an op, if the player is an op it returns true.
//	 * ESM defaults that all op users can use all commands registered with ESM.
//	 * @param sender
//	 * @return true if the user is an op.
//	 */
//	public static boolean isOp(ICommandSender sender) {
//		String[] ops = MinecraftServer.getServer().getConfigurationManager().func_152606_n();
//		for (String op : ops) {
//			if (op.contains(sender.getCommandSenderName())) {
//				return true;
//			}
//		}
//		return false;
//	}
//	
//	/**
//	 * This method gets the config permission level from the config file.
//	 * @return the int that is defined in the ESM config file, or defaults to 3.
//	 */
//	public static int getRequiredPermissionLevel() {
//		findMethods();
//		if (getRequiredPermLevel != null) {
//			try {
//				return (Integer)getRequiredPermLevel.invoke(null);
//			} catch (Exception e) {
//				
//			}
//		}
//		return 3;
//	}
//	
//	/**
//	 * This method gets the methods and classes using reflection to get all the classes handled by
//	 * the ESM API.
//	 */
//	private static void findMethods() {
//		if (!searched) {
//			try {
//				esmCommandCore = Class.forName("net.mysticdrew.esm.core.EsmCommandCore");
//				
//				canPlayerUseCommand = findEsmCoreMethod("canPlayerUseCommand", new Class[]{
//						ICommandSender.class, String.class
//				});
//				
//				getRequiredPermLevel = findEsmCoreMethod("getRequiredPermLevel", new Class[]{});
//				
//			} catch (Exception e) {
//				System.out.println("[EnhancedServerModerationAPI] ESM not found!");
//			} finally {
//				searched = true;
//			}
//		}
//	}
//	
//	
//	private static Method findEsmCoreMethod(String name, Class[] args) {
//		try {
//			return esmCommandCore.getMethod(name, args);
//		} catch (NoSuchMethodException nsme) {
//			System.out.println("[EnhancedServerModerationAPI] ESM Method " + name + " not found!");
//			return null;
//		}
//	}
}
