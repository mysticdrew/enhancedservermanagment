/*	EnhancedServerModeration is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    EnhancedServerModeration is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with EnhancedServerModeration.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.mysticdrew.esm;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import net.minecraftforge.common.MinecraftForge;
import net.mysticdrew.esm.commands.Played;
import net.mysticdrew.esm.commands.SetColor;
import net.mysticdrew.esm.commands.admin.AddUser;
import net.mysticdrew.esm.commands.admin.AssignGroup;
import net.mysticdrew.esm.commands.admin.EditGroup;
import net.mysticdrew.esm.commands.admin.PlayerInfo;
import net.mysticdrew.esm.commands.admin.PrintCommands;
import net.mysticdrew.esm.commands.admin.RemoveGroup;
import net.mysticdrew.esm.commands.admin.RemoveUser;
import net.mysticdrew.esm.commands.vanilla.Help;
import net.mysticdrew.esm.commands.vanilla.Whitelist;
import net.mysticdrew.esm.configuration.ConfigHandler;
import net.mysticdrew.esm.events.FMLEvents;
import net.mysticdrew.esm.events.ForgeEvents;
import net.mysticdrew.esm.model.Command;
import net.mysticdrew.esm.model.Group;
import net.mysticdrew.esm.model.User;
import net.mysticdrew.esm.network.PacketHandler;
import net.mysticdrew.esm.reference.Reference;
import net.mysticdrew.esm.utils.FileUtils;
import net.mysticdrew.esm.utils.LogHelper;
import net.mysticdrew.esm.utils.ServerUtils;
import net.mysticdrew.esm.utils.user.PlayerUtils;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartedEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.event.FMLServerStoppedEvent;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;


@Mod(modid = Reference.MOD_ID, name=Reference.MOD_NAME, version=Reference.VERSION, acceptableRemoteVersions = "*")


public class EnhancedServerModeration {
	@Instance(Reference.MOD_ID)
    public static EnhancedServerModeration instance;
	public static Map<UUID, User> userMap = new HashMap<UUID, User>();
	public static Map<String, Command> registeredCommands = new HashMap<String, Command>();
	public static Map<String, Group> groupMap = new HashMap<String, Group>();
	public static Set<String> adminCommands = new HashSet<String>();
	public static SimpleNetworkWrapper network;
	public static UUID WORLD_ID;	
	public static boolean disableWorldID;
	
    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
    	blackListedCommands();
    	PacketHandler.init();
    	ConfigHandler.init(event.getSuggestedConfigurationFile());
    	FMLCommonHandler.instance().bus().register(new ConfigHandler());
    	FMLCommonHandler.instance().bus().register(new FMLEvents()); 
    	MinecraftForge.EVENT_BUS.register(new ForgeEvents());
    }

    @EventHandler
    public void init(FMLInitializationEvent event) {
    	FMLCommonHandler.instance().bus().register(new TickHandler());
    	
    }
    
    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {

    }
    
    @EventHandler
    public void serverLoad(FMLServerStartingEvent event) {
    	//esm user commands
    	event.registerServerCommand(new SetColor());
    	event.registerServerCommand(new Played());
    	//vanilla commands
    	event.registerServerCommand(new Whitelist());
    	event.registerServerCommand(new Help());
    	//admin commands
    	event.registerServerCommand(new AddUser());
    	event.registerServerCommand(new RemoveUser());
    	event.registerServerCommand(new PrintCommands());
    	event.registerServerCommand(new PlayerInfo());
    	event.registerServerCommand(new EditGroup());
    	event.registerServerCommand(new AssignGroup());
    	event.registerServerCommand(new RemoveGroup());
    }
    
    
    @EventHandler
    public void serverLoaded(FMLServerStartedEvent event) {
    	disableWorldID = Loader.isModLoaded("JourneyMapServer");
    	
    	removeBlackListedCommandsFromMap();
    	if (!disableWorldID) {
    		WORLD_ID = ServerUtils.instance.getWorldId();
    	} else {
    		LogHelper.info("JourneyMapServer Detected, disabling our WorldID Packet.");
    	}
    }
    
    @EventHandler
    public void serverStopping(FMLServerStoppedEvent event) {
    	//Forcing the server to save players if they have not logged out before the sever went down.
    	for (Map.Entry<UUID, User> s : userMap.entrySet()) {
    		User user = s.getValue();
    		user.setLogoutDate(new Date());
    		user.setTimePlayedInMillis(PlayerUtils.instance.calculateTimePlayed(user, user.getLogoutDate()));
    		FileUtils.instance.saveUser(user);
    	}
    }
    
    private void blackListedCommands() {
    	adminCommands.add("adduser");
    	adminCommands.add("assigngroup");
    	adminCommands.add("editgroup");
    	adminCommands.add("playerinfo");
    	adminCommands.add("printcommands");
    	adminCommands.add("removegroup");
    	adminCommands.add("removeuser");
    	adminCommands.add("op");
    	adminCommands.add("deop");
    	adminCommands.add("stop");
    	//adminCommands.add("gamemode");
    	adminCommands.add("save-all");
    	adminCommands.add("save-off");
    	adminCommands.add("save-on");
    }
    
    private void removeBlackListedCommandsFromMap(){
//    	for (String s : adminCommands) {
//    		registeredCommands.remove(s);
//    	}
    }
}
