/*	EnhancedServerModeration is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    EnhancedServerModeration is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with EnhancedServerModeration.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.mysticdrew.esm.events;

import java.util.Date;
import java.util.UUID;

import net.minecraft.server.MinecraftServer;
import net.mysticdrew.esm.model.User;
import net.mysticdrew.esm.utils.FileUtils;
import net.mysticdrew.esm.utils.LogHelper;
import net.mysticdrew.esm.utils.user.PlayerUtils;
import net.mysticdrew.esm.utils.user.UserEventManager;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent.PlayerLoggedOutEvent;
import cpw.mods.fml.common.network.FMLNetworkEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class FMLEvents {
	
	@SideOnly(Side.SERVER)
	@SubscribeEvent
	public void logInEvent(PlayerLoggedInEvent event) {
		UserEventManager.instance.playerLogin(event.player);
	}
	
	@SideOnly(Side.SERVER)
	@SubscribeEvent
	public void logOutEvent(PlayerLoggedOutEvent event) {
		LogHelper.info(event.player.getCommandSenderName() + " logged out!");
		UUID uuid = event.player.getUniqueID();
		User user = FileUtils.instance.loadUser(uuid);
		if (user != null) {
			user.setLogoutDate(new Date());
			user.setTimePlayedInMillis(PlayerUtils.instance.calculateTimePlayed(user, user.getLogoutDate()));
			LogHelper.info(PlayerUtils.instance.timePlayed(user.getTimePlayedInMillis()));
			FileUtils.instance.saveUser(user);
			PlayerUtils.instance.removeUserFromMap(uuid);
		}
	}
}
