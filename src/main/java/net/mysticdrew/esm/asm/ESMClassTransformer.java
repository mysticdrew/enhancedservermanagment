package net.mysticdrew.esm.asm;

import java.io.IOException;
import java.util.List;

import net.minecraft.launchwrapper.IClassTransformer;
import net.minecraft.launchwrapper.LaunchClassLoader;
import net.mysticdrew.esm.utils.LogHelper;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodNode;

public class ESMClassTransformer implements IClassTransformer {
	private ClassTransformerPatches cts = ClassTransformerPatches.instance;
	private List<ClassTransformerPatches.ClassPatch> patches = cts.patch;
	
	
	@Override
	public byte[] transform(String name, String transformedName, byte[] bytes) {
		for (ClassTransformerPatches.ClassPatch patch : patches) {
			if (patch.getClassName().equals(transformedName)) {
				LogHelper.info("[ESM.ASM] Class found: " + transformedName);
				return patch(patch, name, bytes);
			}
		}
		return bytes;
	}	
	
	private byte[] patch(ClassTransformerPatches.ClassPatch patch, String name, byte[] bytes) {
		ClassNode classNode = new ClassNode();
        ClassReader classReader = new ClassReader(bytes);
        List<ClassTransformerPatches.Methods> methodsForPatching = patch.getMethodList();
        MethodNode vanillaMethod = null;
        MethodNode customMethod = null;
        classReader.accept(classNode, 0);
		for (ClassTransformerPatches.Methods method : methodsForPatching) {
			vanillaMethod = getVanillaMethod(method, classNode);
			customMethod = getReplacementMethod(method, patch.getNewClassName());
			
			if (vanillaMethod != null && customMethod != null) {
				LogHelper.info("[ESM.ASM] removing method: " + vanillaMethod.name);
				classNode.methods.remove(vanillaMethod);
				LogHelper.info("[ESM.ASM] replacing it with: " + customMethod.name);
				classNode.methods.add(customMethod);
			} else {
				return bytes;
			}
		}
		ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_MAXS);
        classNode.accept(writer);
        return writer.toByteArray();
	}
	
	private MethodNode getVanillaMethod(ClassTransformerPatches.Methods method, ClassNode classNode) {
		for (MethodNode methodNode : classNode.methods) {
			if ((methodNode.name.equals(method.getDeobfName()) 
					|| methodNode.name.equals(method.getObfName())) 
					&& methodNode.desc.equals(method.getDesc())) {

				LogHelper.info("[ESM.ASM] found vanilla method: " + methodNode.name);
				return methodNode;
			} 
		}
		return null;
	}
	
	private MethodNode getReplacementMethod(ClassTransformerPatches.Methods method, String className) {
		try {
			LaunchClassLoader loader = (LaunchClassLoader) ESMClassTransformer.class.getClassLoader();
	        ClassNode classNode = new ClassNode();
	        ClassReader classReader;
			classReader = new ClassReader(loader.getClassBytes(className));
			classReader.accept(classNode, 0);
			for (MethodNode methodNode : classNode.methods) {
				if ((methodNode.name.equals(method.getDeobfName()) 
						|| methodNode.name.equals(method.getObfName()))
						&& methodNode.desc.equals(method.getDesc())) {
					
					LogHelper.info("[ESM.ASM] found replacement method: " + methodNode.name);
					return methodNode;
				} 
			}
		} catch (IOException e) {
			LogHelper.error("ClassReader IOException: " + e);
			return null;
		}
		return null;
	}	
}
