package net.mysticdrew.esm.asm;

import java.util.ArrayList;
import java.util.List;

import net.mysticdrew.esm.utils.LogHelper;

public class ClassTransformerPatches {
	public static ClassTransformerPatches instance = new ClassTransformerPatches();
	public List<ClassPatch> patch = new ArrayList<ClassPatch>();
	
	public ClassTransformerPatches() {
		LogHelper.info("Creating patch for CommandHandler");
		patch.add(retriveCommandHandlerPatch());
	}
	
	private ClassPatch retriveCommandHandlerPatch() {
		ClassPatch patch = new ClassPatch("net.minecraft.command.CommandHandler", "net.mysticdrew.esm.handlers.ESMCommandHandler");
		patch.addMethod("func_71556_a","executeCommand",  "(Lnet/minecraft/command/ICommandSender;Ljava/lang/String;)I");
		patch.addMethod("func_71560_a", "registerCommand", "(Lnet/minecraft/command/ICommand;)Lnet/minecraft/command/ICommand;");
		patch.addMethod("func_71557_a", "getPossibleCommands", "(Lnet/minecraft/command/ICommandSender;)Ljava/util/List;");
		return patch;
	}
	
	public static class ClassPatch {
		private String className;
		private String newClassName;
		private List<Methods> methodList;
		
		public ClassPatch(String className, String newClassName) {
			this.className = className;
			this.newClassName = newClassName;
			methodList = new ArrayList<Methods>();
		}
		
		public String getClassName() {
			return className;
		}

		public void setClassName(String className) {
			this.className = className;
		}

		public String getNewClassName() {
			return newClassName;
		}

		public void setNewClassName(String newClassName) {
			this.newClassName = newClassName;
		}

		public List<Methods> getMethodList() {
			return methodList;
		}

		public void setMethodList(List<Methods> methodList) {
			this.methodList = methodList;
		}
		
		public void addMethod(String obfName, String deobfName, String desc) {
			methodList.add(new Methods(obfName, deobfName, desc));
		}
	}
	
	public static class Methods {
		private String obfName;
		private String deobfName;
		private String desc;

		public Methods(String obfName, String deobfName, String desc) {
			this.obfName = obfName;
			this.deobfName = deobfName;
			this.desc = desc;
		}

		public String getObfName() {
			return obfName;
		}

		public void setObfName(String obfName) {
			this.obfName = obfName;
		}

		public String getDeobfName() {
			return deobfName;
		}

		public void setDeobfName(String deobfName) {
			this.deobfName = deobfName;
		}

		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}
	}
}
