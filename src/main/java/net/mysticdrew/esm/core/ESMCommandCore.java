/*	EnhancedServerModeration is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    EnhancedServerModeration is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with EnhancedServerModeration.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.mysticdrew.esm.core;

import java.util.HashSet;
import java.util.Set;

import com.mojang.authlib.GameProfile;

import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.mysticdrew.esm.EnhancedServerModeration;
import net.mysticdrew.esm.configuration.ConfigHandler;
import net.mysticdrew.esm.model.User;
import net.mysticdrew.esm.utils.GroupUtils;
import net.mysticdrew.esm.utils.LogHelper;
import net.mysticdrew.esm.utils.user.PlayerUtils;

public class ESMCommandCore {
	
	public static boolean canPlayerUseCommand(ICommandSender sender, String command) {
		GameProfile playerProfile = PlayerUtils.instance.getUserByName(sender.getCommandSenderName());
		if (EnhancedServerModeration.userMap.containsKey(playerProfile.getId())) {
			User user = EnhancedServerModeration.userMap.get(playerProfile.getId());
			Set<String> playerCommands = new HashSet<String>(user.getCommandList());
			if (playerCommands.size() > 0) {
				if (playerCommands.contains(command)) {
					LogHelper.info(sender.getCommandSenderName() + " used command: " + command);
					return true;
				} else if (GroupUtils.instance.validateUserGroupCommand(playerProfile.getId(), command)) {
					LogHelper.info(sender.getCommandSenderName() + " used command: " + command);
					return true;
				}
			}
			return false;
		}
		if (PlayerUtils.instance.isOp(sender)) {
			LogHelper.info(sender.getCommandSenderName() + " used command: " + command);
			return true;
		} else if (sender instanceof MinecraftServer){
			LogHelper.info(sender.getCommandSenderName() + " used command: " + command);
			return true;
		} else {
			LogHelper.info(sender.getCommandSenderName() + " failed to use command: " + command);
			return false;
		}
	}

	public static int getRequiredPermLevel() {
		return ConfigHandler.adminLevel;
	}

}
