package net.mysticdrew.esm.commands.admin;

import java.util.Date;
import java.util.List;

import net.minecraft.command.ICommandSender;
import net.minecraft.util.StatCollector;
import net.mysticdrew.esm.model.User;
import net.mysticdrew.esm.utils.ChatUtils;
import net.mysticdrew.esm.utils.FileUtils;
import net.mysticdrew.esm.utils.ServerUtils;
import net.mysticdrew.esm.utils.user.PlayerUtils;

import com.mojang.authlib.GameProfile;

public class PlayerInfo extends AdminCommandBase {
	private static final String COMMAND_NAME = "playerinfo";
	private static final String COMMAND_USAGE =  StatCollector.translateToLocal("command.player-info.usage");
	private static final String DETAILS = StatCollector.translateToLocal("command.player-info.details");
	private static final String UUID = StatCollector.translateToLocal("general.uuid.text");
	private static final String COMMANDS_TEXT = StatCollector.translateToLocal("general.commands.text");
	private static final String NO_COMMANDS_ASSIGNED = StatCollector.translateToLocal("command.player-info.nocommands");
	private static final String NO_GROUPS_ASSIGNED = StatCollector.translateToLocal("command.player-info.nogroups");
	private static final String GROUPS_TEXT = StatCollector.translateToLocal("general.groups.text");
	private static final String DIED_TEXT = StatCollector.translateToLocal("general.died.text") ;
	private static final String TIMES_TEXT = StatCollector.translateToLocal("general.times.text");
	private static final String WARNINGS = StatCollector.translateToLocal("command.player-info.warnings");
	private static final String KICKS = StatCollector.translateToLocal("command.player-info.kicks");
	private static final String BANS = StatCollector.translateToLocal("command.player-info.bans");
	private static final String NOT_ADDED = StatCollector.translateToLocal("command.player-info.notadded");
	private static final String USER_NOT_EXIST = StatCollector.translateToLocal("general.user.notexist");
	private static final String USER_IP = StatCollector.translateToLocal("command.player-info.ip");
	private static final String LOGGED_IN_AT = StatCollector.translateToLocal("command.player-info.loggedin");
	private static final String ONLINE_FOR = StatCollector.translateToLocal("command.player-info.loggedinfor");
	private static final String LAST_LOGIN = StatCollector.translateToLocal("command.player-info.lastloggedin");
	private static final String LOGGED_OUT_AT = StatCollector.translateToLocal("command.player-info.loggedout");
	private static final String ONLINE_TIME = StatCollector.translateToLocal("command.player-info.wasonlinefor");
	private static final String TOTAL_TIME = StatCollector.translateToLocal("command.player-info.totalTime");
	private static final String NEVER_JOINED = StatCollector.translateToLocal("command.player-info.neverjoined");
	

	@Override
	public String getCommandName() {
		return COMMAND_NAME;
	}

	@Override
	public String getCommandUsage(ICommandSender sender) {
		return COMMAND_USAGE;
	}

	@Override
	public void processCommand(ICommandSender sender, String[] args) {
		PlayerUtils pu = PlayerUtils.instance;
		if (args.length == 1) {
			GameProfile gameProfile = PlayerUtils.instance.getUserByName(args[0]);
			if (gameProfile != null) {
				User user = FileUtils.instance.loadUser(gameProfile.getId());
				if (user != null) {
					Long bans = (user.getBans() != null ? user.getBans() : 0);
					Long warns = (user.getWarnings() != null ? user.getWarnings() : 0);
					Long kicks = (user.getKicks() != null ? user.getKicks() : 0);
					Long deaths = (user.getDeaths() != null ? user.getDeaths() : 0);
					ChatUtils.instance.sendChat(sender, DETAILS + user.getName());
					ChatUtils.instance.sendChat(sender, UUID + user.getId());
					ChatUtils.instance.sendChat(sender, COMMANDS_TEXT + printList(user.getCommandList(), NO_COMMANDS_ASSIGNED));
					ChatUtils.instance.sendChat(sender, GROUPS_TEXT + printList(user.getGroupList(), NO_GROUPS_ASSIGNED));
					displayDates(sender, user);
					ChatUtils.instance.sendChat(sender,  DIED_TEXT + deaths + TIMES_TEXT);
					ChatUtils.instance.sendChat(sender, WARNINGS + warns + KICKS + kicks + BANS + bans + ") <--Tracking not implemented yet...");
				} else {
					ChatUtils.instance.sendChat(sender, args[0] + NOT_ADDED);
					ChatUtils.instance.sendChat(sender, UUID  + gameProfile.getId());
				}
			} else {
				ChatUtils.instance.sendChat(sender, args[0] + USER_NOT_EXIST);
			}
		} else {
			ChatUtils.instance.sendChat(sender, COMMAND_USAGE);
		}
			
	}
	
	private String printList(List<String> list, String defaultText) {
		if (list != null) {
			if (list.size() > 0) {
				return list.toString();
			}
		} 
		return defaultText;
	}
	
	private void displayDates(ICommandSender sender, User user) {
		PlayerUtils pu = PlayerUtils.instance;
		if (user.getLoginDate() != null) { 
			ChatUtils.instance.sendChat(sender, USER_IP + user.getPlayerIP());
			if (pu.getOnlinePlayers().contains(user.getName())) {
				ChatUtils.instance.sendChat(sender, LOGGED_IN_AT  + user.getLoginDate());
				ChatUtils.instance.sendChat(sender, ONLINE_FOR  + pu.getDateDifference(user.getLoginDate(), new Date()));
				ChatUtils.instance.sendChat(sender, TOTAL_TIME + pu.timePlayed(pu.calculateTimePlayed(user, new Date())));
			} else {
				ChatUtils.instance.sendChat(sender, LAST_LOGIN + user.getLoginDate());
				ChatUtils.instance.sendChat(sender, LOGGED_OUT_AT + user.getLogoutDate());
				ChatUtils.instance.sendChat(sender, ONLINE_TIME + pu.getDateDifference(user.getLoginDate(), user.getLogoutDate()));
				ChatUtils.instance.sendChat(sender, TOTAL_TIME + pu.timePlayed(user.getTimePlayedInMillis()));
			}
		} else {
			ChatUtils.instance.sendChat(sender, user.getName() + NEVER_JOINED);
		}
	}
	
	public List addTabCompletionOptions(ICommandSender sender, String[] args) {
		if (args.length == 1) {
			/**
			 * Returns a List of strings (chosen from the given strings) which
			 * the last word in the given string array is a beginning-match for.
			 * (Tab completion).
			 */
			return getListOfStringsMatchingLastWord(args,
					ServerUtils.instance.getWorldUsers());
		} else {
			return null;
		}
	}
}
