/*	EnhancedServerModeration is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    EnhancedServerModeration is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with EnhancedServerModeration.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.mysticdrew.esm.commands.admin;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.mojang.authlib.GameProfile;

import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.util.StatCollector;
import net.mysticdrew.esm.model.User;
import net.mysticdrew.esm.utils.ChatUtils;
import net.mysticdrew.esm.utils.LogHelper;
import net.mysticdrew.esm.utils.FileUtils;
import net.mysticdrew.esm.utils.ServerUtils;
import net.mysticdrew.esm.utils.user.PlayerUtils;

public class RemoveGroup  extends AdminCommandBase {
	private static final String COMMAND_NAME = "removegroup";
	private static final String COMMAND_USAGE =  StatCollector.translateToLocal("command.remove-group.usage");
	private static final String GROUP_NOT_FOUND = StatCollector.translateToLocal("genera.group.notfound");
	private static final String GROUP_NOT_EXIST = StatCollector.translateToLocal("general.user.notexist");
	private static final String REMOVED_FROM = StatCollector.translateToLocal("command.remove-user.removed");
	private static final String USER_ERROR = StatCollector.translateToLocal("general.user.error");
	
	@Override
	public String getCommandName() {
		return COMMAND_NAME;
	}
	@Override
	public String getCommandUsage(ICommandSender sender) {
		return COMMAND_USAGE;
	}
	@Override
	public void processCommand(ICommandSender sender, String[] args) {
		if (args.length > 1) {
			removeUserCommands(sender, args);
		} else {
			ChatUtils.instance.sendChat(sender, COMMAND_USAGE);
		}
	}
	
	private void removeUserCommands(ICommandSender sender, String[] args) {
		GameProfile userProfile = PlayerUtils.instance.getUserByName(args[0]);
		Set<String> grpSet = null;
		List<String> groups = new ArrayList<String>();

		if (userProfile != null) {
			User user = FileUtils.instance.loadUser(userProfile.getId());
			if (user != null) {
				grpSet = new HashSet<String>(user.getGroupList());
			} else {
				LogHelper.error(args[0] + USER_ERROR);
				return;
			}

			if (args.length > 1) {
				for (int i = 1; i < args.length; i++) {
					if (FileUtils.instance.loadGroup(args[i]) != null) {
						grpSet.remove(args[i]);
						groups.add(args[i]);
					} else {
						ChatUtils.instance.sendChat(sender, GROUP_NOT_FOUND + args[i]);
					}
				}
				ChatUtils.instance.sendChat(sender, groups + REMOVED_FROM + args[0]);
				user.setGroupList(new ArrayList<String>(grpSet));
				saveUser(user);
			}
			
		} else {
			new WrongUsageException(args[0] + GROUP_NOT_EXIST);
		}
	}
	
	public List addTabCompletionOptions(ICommandSender sender, String[] args) {
		if (args.length == 1) {
			/**
			 * Returns a List of strings (chosen from the given strings) which
			 * the last word in the given string array is a beginning-match for.
			 * (Tab completion).
			 */
			return getListOfStringsMatchingLastWord(args,
					ServerUtils.instance.getWorldUsers());
		} else {
			if (args.length > 1) {

				/**
				 * Returns a List of strings (chosen from the given strings)
				 * which the last word in the given string array is a
				 * beginning-match for. (Tab completion).
				 */
				return getListOfStringsMatchingLastWord(args, ServerUtils.instance.getWorldGroups());
			}

			return null;
		}
	}
}
