/*	EnhancedServerModeration is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    EnhancedServerModeration is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with EnhancedServerModeration.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.mysticdrew.esm.commands.admin;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.minecraft.command.ICommandSender;
import net.minecraft.util.StatCollector;
import net.mysticdrew.esm.EnhancedServerModeration;
import net.mysticdrew.esm.model.User;
import net.mysticdrew.esm.utils.ChatUtils;
import net.mysticdrew.esm.utils.FileUtils;
import net.mysticdrew.esm.utils.ServerUtils;
import net.mysticdrew.esm.utils.user.PlayerUtils;

import com.mojang.authlib.GameProfile;

public class RemoveUser  extends AdminCommandBase {
	private static final String COMMAND_NAME = "removeuser";
	private static final String COMMAND_USAGE =  StatCollector.translateToLocal("command.remove-user.usage");
	private static final String COMMAND_NOT_FOUND = StatCollector.translateToLocal("general.command.notfound");
	private static final String USER_NOT_EXIST = StatCollector.translateToLocal("general.user.notexist");
	private static final String REMOVED_FROM = StatCollector.translateToLocal("command.remove-user.removed");
	
	@Override
	public String getCommandName() {
		return COMMAND_NAME;
	}
	@Override
	public String getCommandUsage(ICommandSender sender) {
		return COMMAND_USAGE;
	}
	@Override
	public void processCommand(ICommandSender sender, String[] args) {
		if (args.length > 1) {
			removeUserCommands(sender, args);
		} else {
			ChatUtils.instance.sendChat(sender, COMMAND_USAGE);
		}
	}
	
	private void removeUserCommands(ICommandSender sender, String[] args) {
		EnhancedServerModeration esm = EnhancedServerModeration.instance;
		GameProfile userProfile = PlayerUtils.instance.getUserByName(args[0]);
		Set<String> cmdSet;
		List<String> commands = new ArrayList<String>();
		
		if (userProfile != null) {
			User user = FileUtils.instance.loadUser(userProfile.getId());
			if (user != null) {
				cmdSet = new HashSet<String>(user.getCommandList());
			} else {
				ChatUtils.instance.sendChat(sender, args[0] + USER_NOT_EXIST);
				return;
			}
			if (args.length > 1) {
				for (int i = 1; i < args.length; i++) {
					if (esm.registeredCommands.containsKey(args[i])
							&& cmdSet.contains(args[i])) {
						cmdSet.remove(args[i]);
						commands.add(args[i]);
					} else {
						ChatUtils.instance.sendChat(sender, COMMAND_NOT_FOUND + args[i]);
					}
				}
			}
			ChatUtils.instance.sendChat(sender, commands + REMOVED_FROM + args[0]);
			user.setCommandList(new ArrayList<String>(cmdSet));
			saveUser(user);
		} else {
			ChatUtils.instance.sendChat(sender, args[0] + USER_NOT_EXIST);
		}
	}
	
	public List addTabCompletionOptions(ICommandSender sender, String[] args) {
		if (args.length == 1) {
			/**
			 * Returns a List of strings (chosen from the given strings) which
			 * the last word in the given string array is a beginning-match for.
			 * (Tab completion).
			 */
			return getListOfStringsMatchingLastWord(args,
					ServerUtils.instance.getWorldUsers());
		} else {
			if (args.length > 1) {

				/**
				 * Returns a List of strings (chosen from the given strings)
				 * which the last word in the given string array is a
				 * beginning-match for. (Tab completion).
				 */
				return getListOfStringsMatchingLastWord(args,
						ServerUtils.instance.getCommandList());
			}

			return null;
		}
	}
}
