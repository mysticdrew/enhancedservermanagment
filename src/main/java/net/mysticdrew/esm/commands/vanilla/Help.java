package net.mysticdrew.esm.commands.vanilla;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandNotFoundException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.NumberInvalidException;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.event.ClickEvent;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.MathHelper;

public class Help extends CommandBase
{
    private static final String __OBFID = "CL_00000529";

    public String getCommandName()
    {
        return "help";
    }

    /**
     * Return the required permission level for this command.
     */
    public int getRequiredPermissionLevel()
    {
        return 0;
    }

    public String getCommandUsage(ICommandSender p_71518_1_)
    {
        return "commands.help.usage";
    }

    public List getCommandAliases()
    {
        return Arrays.asList(new String[] {"?"});
    }

    public void processCommand(ICommandSender sender, String[] args)
    {
        List list = this.getSortedPossibleCommands(sender);
        byte firstByte = 7;
        int index = (list.size() - 1) / firstByte;
        boolean flag = false;
        int length;

        try
        {
        	length = args.length == 0 ? 0 : parseIntBounded(sender, args[0], 1, index + 1) - 1;
        }
        catch (NumberInvalidException numberinvalidexception)
        {
            Map map = this.getCommands();
            ICommand icommand = (ICommand)map.get(args[0]);
                       
            if (icommand != null)
            {
                throw new WrongUsageException(icommand.getCommandUsage(sender), new Object[0]);
            }

            if (MathHelper.parseIntWithDefault(args[0], -1) != -1)
            {
                throw numberinvalidexception;
            }

            throw new CommandNotFoundException();
        }

        int j = Math.min((length + 1) * firstByte, list.size());
        ChatComponentTranslation chatcomponenttranslation1 = new ChatComponentTranslation("commands.help.header", new Object[] {Integer.valueOf(length + 1), Integer.valueOf(index + 1)});
        chatcomponenttranslation1.getChatStyle().setColor(EnumChatFormatting.DARK_GREEN);
        sender.addChatMessage(chatcomponenttranslation1);

        for (int l = length * firstByte; l < j; ++l)
        {
        	try {
        		ICommand icommand1 = (ICommand)list.get(l);
                ChatComponentTranslation chatcomponenttranslation = new ChatComponentTranslation(icommand1.getCommandUsage(sender), new Object[0]);
                chatcomponenttranslation.getChatStyle().setChatClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/" + icommand1.getCommandName() + " "));
                sender.addChatMessage(chatcomponenttranslation);
        	} catch (Exception e) {
        		
        	}
            
        }

        if (length == 0 && sender instanceof EntityPlayer)
        {
            ChatComponentTranslation chatcomponenttranslation2 = new ChatComponentTranslation("commands.help.footer", new Object[0]);
            chatcomponenttranslation2.getChatStyle().setColor(EnumChatFormatting.GREEN);
            sender.addChatMessage(chatcomponenttranslation2);
        }
    }

    /**
     * Returns a sorted list of all possible commands for the given ICommandSender.
     */
	protected List<ICommand> getSortedPossibleCommands(ICommandSender sender) {
		List<ICommand> fixedList = new ArrayList<ICommand>();
		for (Object command : MinecraftServer.getServer().getCommandManager().getPossibleCommands(sender)) {
			fixedList.add((ICommand) command);
		}
		
		Collections.sort(fixedList, new Comparator<ICommand>() {
			@Override
			public int compare(ICommand c1, ICommand c2) {
				return c1.getCommandName().compareTo(c2.getCommandName());
			}
		});
		return fixedList;
	}

    protected Map getCommands()
    {
        return MinecraftServer.getServer().getCommandManager().getCommands();
    }
}