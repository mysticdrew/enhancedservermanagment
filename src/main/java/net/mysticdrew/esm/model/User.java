package net.mysticdrew.esm.model;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import net.minecraft.util.EnumChatFormatting;

/*	EnhancedServerModeration is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

EnhancedServerModeration is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with EnhancedServerModeration.  If not, see <http://www.gnu.org/licenses/>.
*/
public class User {
	private String name;
	private UUID id;
	private List<String> commandList;
	private List<String> groupList;
	private Date loginDate;
	private Date logoutDate;
	private String playerIP;
	private Long timePlayedInMillis;
	private Long deaths;
	private Long bans;
	private Long warnings;
	private Long kicks;
	private EnumChatFormatting nameColor;
	
	public EnumChatFormatting getNameColor() {
		return this.nameColor;
	}
	
	public void setNameColor(EnumChatFormatting nameColor) {
		this.nameColor = nameColor;
	}
	
	public Long getDeaths() {
		return deaths;
	}

	public void setDeaths(Long deaths) {
		this.deaths = deaths;
	}

	public Long getBans() {
		return bans;
	}

	public void setBans(Long bans) {
		this.bans = bans;
	}

	public Long getWarnings() {
		return warnings;
	}

	public void setWarnings(Long warnings) {
		this.warnings = warnings;
	}

	public Long getKicks() {
		return kicks;
	}

	public void setKicks(Long kicks) {
		this.kicks = kicks;
	}

	public Long getTimePlayedInMillis() {
		return timePlayedInMillis;
	}

	public void setTimePlayedInMillis(Long timePlayedInMillis) {
		this.timePlayedInMillis = timePlayedInMillis;
	}

	public String getPlayerIP() {
		return playerIP;
	}

	public void setPlayerIP(String playerIP) {
		this.playerIP = playerIP;
	}

	public Date getLoginDate() {
		return loginDate;
	}

	public void setLoginDate(Date loginDate) {
		this.loginDate = loginDate;
	}

	public Date getLogoutDate() {
		return logoutDate;
	}

	public void setLogoutDate(Date logoutDate) {
		this.logoutDate = logoutDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public List<String> getCommandList() {
		return commandList;
	}

	public void setCommandList(List<String> commandList) {
		this.commandList = commandList;
	}

	public List<String> getGroupList() {
		return groupList;
	}

	public void setGroupList(List<String> groupList) {
		this.groupList = groupList;
	}

	@Override
	public String toString() {
		return "User [name=" + name + ", uuid=" + id + ", commandsList="
				+ commandList + ", groupList=" + groupList + "]";
	}

}
