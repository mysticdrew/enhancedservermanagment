package net.mysticdrew.esm.model;

import java.util.HashSet;
import java.util.Set;


public class Command {
	private String commandName;
	private String commandUsage;
	private String commandOwner;
	private static final Set<String> vanillaCommands  = new HashSet<String>();
	
	public Command() {
		vanillaCommandList();
	}

	public String getCommandName() {
		return commandName;
	}

	public void setCommandName(String commandName) {
		this.commandName = commandName;
	}

	public String getCommandUsage() {
		return commandUsage;
	}

	public void setCommandUsage(String commandUsage) {
		this.commandUsage = commandUsage;
	}

	public String getCommandOwner() {
		return getRealOwner(commandOwner);
	}

	public void setCommandOwner(String commandOwner) {
		this.commandOwner = commandOwner;
	}
	
	private String getRealOwner(String owner) {
		if (vanillaCommands.contains(commandName)) {
			return "Minecraft";
		} else {
			return owner;
		}
	}
	
	private void vanillaCommandList() {
		vanillaCommands.add("achievement");
		vanillaCommands.add("ban");
		vanillaCommands.add("ban-ip");
		vanillaCommands.add("banlist");
		vanillaCommands.add("clear");
		vanillaCommands.add("debug");
		vanillaCommands.add("defaultgamemode");
		vanillaCommands.add("difficulty");
		vanillaCommands.add("effect");
		vanillaCommands.add("enchant");
		vanillaCommands.add("gamemode");
		vanillaCommands.add("gamerule");
		vanillaCommands.add("give");
		vanillaCommands.add("help");
		vanillaCommands.add("kick");
		vanillaCommands.add("kill");
		vanillaCommands.add("list");
		vanillaCommands.add("me");
		vanillaCommands.add("netstat");
		vanillaCommands.add("pardon");
		vanillaCommands.add("pardon-ip");
		vanillaCommands.add("playsound");
		vanillaCommands.add("say");
		vanillaCommands.add("scoreboard");
		vanillaCommands.add("seed");
		vanillaCommands.add("setblock");
		vanillaCommands.add("setidletimeout");
		vanillaCommands.add("setworldspawn");
		vanillaCommands.add("spawnpoint");
		vanillaCommands.add("spreadplayers");
		vanillaCommands.add("summon");
		vanillaCommands.add("tell");
		vanillaCommands.add("tellraw");
		vanillaCommands.add("testfor");
		vanillaCommands.add("testforblock");
		vanillaCommands.add("time");
		vanillaCommands.add("toggledownfall");
		vanillaCommands.add("tp");
		vanillaCommands.add("weather");
		vanillaCommands.add("xp");
		vanillaCommands.add("op");
		vanillaCommands.add("deop");
		vanillaCommands.add("stop");
		vanillaCommands.add("save-all");
		vanillaCommands.add("save-off");
    	vanillaCommands.add("save-on");
	}
}
