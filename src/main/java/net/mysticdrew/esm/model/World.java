package net.mysticdrew.esm.model;

import java.util.Set;
import java.util.UUID;

public class World {
	private UUID worldId;
	private Set<String> users;
	private Set<String> groups;

	public Set<String> getUsers() {
		return users;
	}

	public void setUsers(Set<String> users) {
		this.users = users;
	}

	public Set<String> getGroups() {
		return groups;
	}

	public void setGroups(Set<String> groups) {
		this.groups = groups;
	}

	public UUID getWorldId() {
		return worldId;
	}

	public void setWorldId(UUID newId) {
		this.worldId = newId;
	}
	
	
}
