/*	EnhancedServerModeration is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    EnhancedServerModeration is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with EnhancedServerModeration.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.mysticdrew.esm.model;

import java.util.List;

public class Group {
	private String name;
	private List<String> commandsList;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getCommandsList() {
		return commandsList;
	}

	public void setCommandsList(List<String> commandsList) {
		this.commandsList = commandsList;
	}


	@Override
	public String toString() {
		return "Group [name=" + name + ", commandsList=" + commandsList
				+ "]";
	}

}
