/*	EnhancedServerModeration is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    EnhancedServerModeration is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with EnhancedServerModeration.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.mysticdrew.esm.utils.user;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import com.mojang.authlib.GameProfile;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.StatCollector;
import net.mysticdrew.esm.EnhancedServerModeration;
import net.mysticdrew.esm.model.User;
import net.mysticdrew.esm.utils.FileUtils;

public class PlayerUtils {
	
	public static PlayerUtils instance = new PlayerUtils();
	
	public boolean isOp(ICommandSender sender) {
		String[] ops = MinecraftServer.getServer().getConfigurationManager().func_152606_n();
		for (String op : ops) {
			if (op.contains(sender.getCommandSenderName())) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isOp(String player) {
		String[] ops = MinecraftServer.getServer().getConfigurationManager().func_152606_n();
		for (String op : ops) {
			if (op.contains(player)) {
				return true;
			}
		}
		return false;
	}

	public GameProfile getUserByName(String name) {
		MinecraftServer server = MinecraftServer.getServer();
		GameProfile gameProfile = server.func_152358_ax().func_152655_a(name);
		return gameProfile;
	}
	
	
	/**
	 * Gets the player's GameProfile by their UUID.
	 * 
	 * @param uuid
	 * @return {@link com.mojang.authlib.GameProfile}
	 */
	public GameProfile getPlayerInfoById(UUID uuid) {
		MinecraftServer server = MinecraftServer.getServer();
		GameProfile gameProfile = server.func_152358_ax().func_152652_a(uuid);
		return gameProfile;
	}

	public void removeUserFromMap(UUID uuid) {
		EnhancedServerModeration.userMap.remove(uuid);
	}

	public Set<String> getOnlinePlayers() {
		Set<String> users =  new HashSet<String>();
		MinecraftServer server = MinecraftServer.getServer();
		for (String s : server.getAllUsernames()) {
			users.add(s);
		}
		return users;
	}

	public String getPlayerIp(String name) {
		MinecraftServer server = MinecraftServer.getServer();
		EntityPlayerMP playerMP = server.getConfigurationManager().func_152612_a(name);
		return playerMP.getPlayerIP();
	}
	
	@Deprecated
	@SideOnly(Side.SERVER)
	public boolean deleteUser(String player) {
		GameProfile user = getUserByName(player);
		if (getOnlinePlayers().contains(player)) {
			removeUserFromMap(user.getId());
		}
		return FileUtils.instance.deleteUser(user.getId());
	}
	
	
	public Long calculateTimePlayed(User user, Date curTime) {
		return (curTime.getTime() - user.getLoginDate().getTime()) + (user.getTimePlayedInMillis() != null ? user.getTimePlayedInMillis() : 0L);
	}
	
	public String timePlayed(Long time) {
		String output = "";
		long second = (time / 1000) % 60;
		long minute = (time / (1000 * 60)) % 60;
		long hour = (time / (1000 * 60 * 60)) % 24;
		long day = (time/  (1000 * 60 * 60 * 24));

		return (day > 0 ? day + StatCollector.translateToLocal("general.time.day"):"")
				+ (hour > 0 ? hour + StatCollector.translateToLocal("general.time.hour"):"")
				+ (minute > 0 ? minute + StatCollector.translateToLocal("general.time.minutes"):"")
				+ (second > 0 ? second + StatCollector.translateToLocal("general.time.Seconds"):"");
	}
	
	public String getDateDifference(Date d1, Date d2) {
		Long diffInMillies = d2.getTime() - d1.getTime();
		return timePlayed(diffInMillies);
	}
	
	public EntityPlayerMP getPlayerEntityByName(String name) {
		MinecraftServer server = MinecraftServer.getServer();
		return server.getConfigurationManager().func_152612_a(name);
	}
}
