package net.mysticdrew.esm.utils;

import java.io.File;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import net.minecraft.server.MinecraftServer;
import net.mysticdrew.esm.EnhancedServerModeration;
import net.mysticdrew.esm.model.Command;
import net.mysticdrew.esm.model.World;
import cpw.mods.fml.common.FMLCommonHandler;

public class ServerUtils {
	private static World world;
	public static ServerUtils instance = new ServerUtils();
	private EnhancedServerModeration esm = EnhancedServerModeration.instance;
	
	public ServerUtils() {
		loadWorld();
	}
	
	public UUID getWorldId() {
//		MinecraftServer server = MinecraftServer.getServer();//7255188712731773569
//		LogHelper.info("Seed: " + server.worldServerForDimension(0).getSeed());
		if (world.getWorldId() == null) {
			LogHelper.info("Creating UUID for the world.");
			UUID newId = UUID.randomUUID();
			world.setWorldId(newId);
			saveWorld();
		}
		LogHelper.info("World UUID: " + world.getWorldId());
		return world.getWorldId();
	}
	
	public void addGroupToWorld(String group) {
		if (world.getGroups() == null) {
			world.setGroups(new HashSet<String>());
		}
		world.getGroups().add(group);
		saveWorld();
	}
	
	public void addUserToWorld(String user) {
		if (world.getUsers() == null) {
			world.setUsers(new HashSet<String>());
		}
		if (!world.getUsers().contains(user)) {
			world.getUsers().add(user);
			saveWorld();
		}
	}
	
	public String[] getWorldUsers() {
		Set<String> userSet =  world.getUsers();
		if (userSet != null) {
			return userSet.toArray(new String[userSet.size()]);	
		} else {
			return null;
		}
	}
	
	public String[] getWorldGroups() {
		Set<String> groupSet =  world.getGroups();
		if (groupSet != null) {
			return groupSet.toArray(new String[groupSet.size()]);	
		} else {
			return null;
		}
	}
	
	public String[] getCommandList() {
		String[] commands = new String[esm.registeredCommands.size()];
		int i = 0;
		for (Map.Entry<String, Command> map : esm.registeredCommands.entrySet()) {
			commands[i] = map.getKey();
			i++;
		}
		return commands;
	}
	
	public void saveWorld() {
		File file = FMLCommonHandler.instance().getMinecraftServerInstance().getEntityWorld().getSaveHandler().getWorldDirectory();
		String path = file.getPath();
		FileUtils.instance.saveWorldFile(path, world);
	}
	
	private void loadWorld() {
		File file = FMLCommonHandler.instance().getMinecraftServerInstance().getEntityWorld().getSaveHandler().getWorldDirectory();
		String path = file.getPath();
		world = FileUtils.instance.loadWorldFile(path);
		if (world == null) {
			world = new World();
			saveWorld();
		}
	}
}
