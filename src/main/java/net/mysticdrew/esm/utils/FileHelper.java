/*	EnhancedServerModeration is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    EnhancedServerModeration is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with EnhancedServerModeration.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.mysticdrew.esm.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


public class FileHelper {
	
	public static String readFile(String path, String fileName) {
		BufferedReader bReader;
		FileInputStream fileIn;
		File file;
		file = new File(path, fileName);
		String row = "";
		StringBuilder fileOutput = new StringBuilder();
		try {
			fileIn = new FileInputStream(file);
			bReader = new BufferedReader(new InputStreamReader(fileIn));
			try {
				while ((row = bReader.readLine()) != null) {
					fileOutput.append(row);
				}
				bReader.close();
			} catch (IOException e) {
				LogHelper.error("Unable to read the JsonFile");
				LogHelper.error("Error" + e);
				return null;
			}
			
			return fileOutput.toString();
		} catch (FileNotFoundException e) {
			LogHelper.info(fileName + " not found!");
			return null;
		}
	}
	
	public static boolean writeFile(String path, String fileName, String text) {

		try {
			File dir = new File(path);
			if (dir.exists() && dir.isDirectory()) { 	
			} else {
				dir.mkdirs();
			}
			
			File file = new File(dir, fileName);
			file.createNewFile();
 
			FileWriter fw = new FileWriter(file);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(text);
			bw.close();
			return true;
		} catch (IOException e) {
			LogHelper.error("Error creating file " + fileName);
			LogHelper.error("Error " + e);
			return false;
		}
	}
	
	public static boolean deleteFile(String path, String filename) {
		try {
			Path myPath = Paths.get(path + filename);
			Files.delete(myPath);
			return true;
		} catch (IOException ioe) {
			LogHelper.warn("Delete path exception " + ioe);
			return false;
		}
	}
}
