package net.mysticdrew.esm.network;

import java.util.UUID;

import net.mysticdrew.esm.EnhancedServerModeration;
import net.mysticdrew.esm.utils.LogHelper;
import net.mysticdrew.esm.utils.user.PlayerUtils;
import io.netty.buffer.ByteBuf;
import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;

public class WorldIdMessage implements IMessage{

	private String worldUid;
	 
    public WorldIdMessage() {}
    
    public WorldIdMessage(UUID worldUid) {
    	this.worldUid = worldUid.toString();
    }
    
    public String getWorldUid()
    {
        return worldUid;
    }

    @Override
    public void fromBytes(ByteBuf buf)
    {

    }

    @Override
    public void toBytes(ByteBuf buf)
    {
        try
        {
            if(worldUid!=null)
            {
                ByteBufUtils.writeUTF8String(buf, worldUid);
            }
        }
        catch(Throwable t)
        {
            LogHelper.error("[toBytes]Failed to read message: " + t);
        }
    }
    
    public static class WorldIdListener implements IMessageHandler<WorldIdMessage, IMessage>
    {
    	
 
        @Override
        public IMessage onMessage(WorldIdMessage message, MessageContext ctx)
        {
        	if (!EnhancedServerModeration.disableWorldID) {
        		LogHelper.info("Sending worldId packet to: " + PlayerUtils.instance.getPlayerInfoById(ctx.getServerHandler().playerEntity.getUniqueID()).getName());
        		PacketHandler.MAP_CHANNEL.sendTo(new WorldIdMessage(EnhancedServerModeration.instance.WORLD_ID), ctx.getServerHandler().playerEntity);	
        	}
            return null;
        }
    }
    
    
}